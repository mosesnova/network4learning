﻿using System.Web;
using System.Web.Mvc;

namespace AngularMultipleScreens
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
