﻿using AngularMultipleScreens.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularMultipleScreens.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public string Insert_Employee(Employee Employe)
        {
            if (Employe != null)
            {
                using (EmployeeEntities Obj = new EmployeeEntities())
                {
                    Obj.Employees.Add(Employe);
                    Obj.SaveChanges();
                    CreateAuditTrail(AuditActionType.Insert, Employe.Emp_Id, DateTime.Now);
                    return "Employee Added Successfully";
                }
            }
            else
            {
                return "Employee Not Inserted! Try Again";
            }
        }
        [HttpPost]
        public string Update_Employee(Employee Employe)
        {

           TempData["Employe"] = Employe;
            return "Navigate";
        }
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult About()
        {
            

            return View();
        }
        public JsonResult PopulateData()
        {
            return Json(TempData["Employe"], JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_AllEmployee()
        {
            using (EmployeeEntities Obj = new EmployeeEntities())
            {
                List<Employee> Emp = Obj.Employees.ToList();
                return Json(Emp, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Get_AllAudit()
        {
            using (EmployeeEntities Obj = new EmployeeEntities())
            {
                List<AuditLog> au = Obj.AuditLogs.ToList();
                return Json(au, JsonRequestBehavior.AllowGet);
            }
        }
        public enum AuditActionType { Insert,Delete,Update } 
        public string CreateAuditTrail(AuditActionType Action, int KeyFieldID, DateTime dt)
        {
            if(Action.ToString() != null)
            {
                AuditLog audit = new AuditLog();
                audit.Action = Action.ToString();
                audit.KeyFieldID = KeyFieldID;
                audit.DateTime = dt.ToString();
                using (EmployeeEntities Obj = new EmployeeEntities())
                {
                    Obj.AuditLogs.Add(audit);
                    Obj.SaveChanges();
                    return "Audit Log Added Successfully";
                }
            }else
            {
                return "";
            }
           
        }
    }
}