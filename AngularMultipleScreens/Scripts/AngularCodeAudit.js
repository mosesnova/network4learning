﻿var app = angular.module("myAppAudit", []);
app.controller("myCtrlAudit", function ($scope, $http) {
    $scope.GetAllAuditData = function () {
        $http({
            method: "get",
            url: "/Home/Get_AllAudit"
        }).then(function (response) {
            $scope.audit = response.data;
        }, function () {
            alert("Error Occur");
        })
    };  
})